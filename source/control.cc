#include "control.h"
#include "tile.h"
#include "display.h"

extern Tile ***board;
bool Control::instanceFlag = false;
Control *Control::instance = NULL;

Control *Control::getInstance() {
	if (!instanceFlag) {
		instance = new Control();
		instanceFlag = true;
	}
	return instance;
}

Control::~Control() {
	instanceFlag = false;
}

void Control::update(std::vector<Object *> *onScreen) {
	std::set<Enemy *>::iterator it;
	for (it = moving.begin(); it != moving.end();) {
		Tile *start = (*it)->getPresentTile();
		bool deleted = false;
		std::vector<Character *>::iterator it2;
		for (it2 = targets.begin(); it2 < targets.end(); ++it2) {
			Tile *end = (*it2)->getPresentTile();
			if ((*it)->onTarget()) {
				(*it)->stopMoving();
				instruction[(*it)].pop();
				if (lineOfSight(*start, *end) || instruction[(*it)].empty()) {
					while (!instruction[(*it)].empty()) {
						instruction[(*it)].pop();
					}
					shooting.insert(*it);
					moving.erase(it++);
					deleted = true;
				} else {
					Tile *step = instruction[(*it)].top();
					std::pair<int, int> dir = getDirection(*start, *step);
					(*it)->getInput(dir.first, dir.second, step);	
				}
			}
		}
		if (!deleted) {
			(*it)->update(onScreen);
			++it;
		}
	}
	for (it = shooting.begin(); it != shooting.end();) {
		Tile *start = (*it)->getPresentTile();
		bool deleted = false;
		std::vector<Character *>::iterator it2;
		for (it2 = targets.begin(); it2 < targets.end(); ++it2) {
			Tile *end = (*it2)->getPresentTile();
			Display *d = Display::getInstance();
			if (!lineOfSight(*start, *end) || !d->getScreen()->collision(**it) ||
				(*it)->checkKeepMoving()) {
				(*it)->stop();
				std::stack<Tile *> path;
				if ((*it)->checkKeepMoving()) {
					path = getFreeSpace(start);
				} else {
					path = findPath(start, end);
				}
				instruction[(*it)] = path;
				instruction[(*it)].pop();
				Tile *step = instruction[(*it)].top();
				std::pair<int, int> dir = getDirection(*start, *step);
				(*it)->getInput(dir.first, dir.second, step);
				(*it)->update(onScreen);
				moving.insert(*it);
				shooting.erase(it++);
				deleted = true;
			} else if ((*it)->isHit() || (*it)->overheated()) {
				(*it)->stop();
				std::stack<Tile *> path;
				path = getFreeSpace(start);
				instruction[(*it)] = path;
				instruction[(*it)].pop();
				Tile *step = instruction[(*it)].top();
				std::pair<int, int> dir = getDirection(*start, *step);
				(*it)->getInput(dir.first, dir.second, step);
				(*it)->update(onScreen);
				moving.insert(*it);
				shooting.erase(it++);
				deleted = true;
			} else {
				(*it)->getInput(0, 0, *it2);
				shoot(*it);
			}
		}
		if (!deleted) {
			(*it)->update(onScreen);
			++it;
		}
	}
}

void Control::shoot(Enemy *e) {
	e->stop(0);
	e->shoot(0);
}

void Control::addControlled(Enemy *c) {
	if (moving.find(c) == moving.end()) {
		shooting.insert(c);
	}
}

void Control::getControlled(std::vector<Object *> *onScreen) {
	std::set<Enemy *>::iterator it;
	for (it = moving.begin(); it != moving.end(); ++it) {
		onScreen->push_back(*it);
	}
	for (it = shooting.begin(); it != shooting.end(); ++it) {
		onScreen->push_back(*it);
	}
}

void Control::addTarget(Character *t) {
	targets.push_back(t);
}

void Control::remove(Enemy *e) {
	moving.erase(e);
	shooting.erase(e);
}

bool Control::activated(Enemy *e) {
	Tile *start = e->getPresentTile();
	std::vector<Character *>::iterator it;
	for (it = targets.begin(); it != targets.end(); ++it) {
		Tile *end = (*it)->getPresentTile();
		return lineOfSight(*start, *end) || e->isHit();
	}
	return false;
}

bool lineOfSight(Tile &start, Tile &end) {
	int dx = abs(end.x - start.x);
	int dy = abs(end.y - start.y);
	int x = start.x;
	int y = start.;
	int n = 1 + dx + dy;
	int xInc = (end.x > start.x) ? 1 : -1;
	int yInc = (end.y > start.y) ? 1 : -1;
	int error = dx - dy;
	dx *= 2;
	dy *= 2;
	for (; n > 0; --n) {
		if (board[x][y]->getHeight() == 2) {
			return false;
		}
		if (error > 0) {
			x += xInc;
			error -= dy;
		} else {
			y += yInc;
			error += dx;
		}
	}
	return true;
}

std::pair<int, int> getDirection(Tile &start, Tile &end) {
	int dx = abs(end.x - start.x);
	int dy = abs(end.y - start.y);
	if (dx >= dy) {
		dx = 1;
		dy = 0;
	} else if (dy > dx) {
		dx = 0;
		dy = 1;
	}
	int xInc = (end.x > start.x) ? 1 : -1;
	int yInc = (end.y > start.y) ? 1 : -1;
	return std::make_pair(dx * xInc, dy * yInc);
}

std::stack<Tile *> getFreeSpace(Tile *e) {
	int r = rand() % 4;
	std::stack<Tile *> path;
	switch(r) {
		case 0:
			if (board[e->x - 1][e->y]->getHeight() == 1) {
				path.push(board[e->x - 1][e->y]);
			} else if (board[e->x + 1][e->y]->getHeight() == 1) {
				path.push(board[e->x + 1][e->y]);
			} else if (board[e->x][e->y + 1]->getHeight() == 1) {
				path.push(board[e->x][e->y + 1]);
			} else if (board[e->x][e->y - 1]->getHeight() == 1) {
				path.push(board[e->x][e->y - 1]);
			}
			break;
		case 1:
			if (board[e->x + 1][e->y]->getHeight() == 1) {
				path.push(board[e->x + 1][e->y]);
			} else if (board[e->x - 1][e->y]->getHeight() == 1) {
				path.push(board[e->x - 1][e->y]);
			} else if (board[e->x][e->y + 1]->getHeight() == 1) {
				path.push(board[e->x][e->y + 1]);
			} else if (board[e->x][e->y - 1]->getHeight() == 1) {
				path.push(board[e->x][e->y - 1]);
			}
			break;		
		case 2:
			if (board[e->x][e->y + 1]->getHeight() == 1) {
				path.push(board[e->x][e->y + 1]);
			} else if (board[e->x + 1][e->y]->getHeight() == 1) {
				path.push(board[e->x + 1][e->y]);
			} else if (board[e->x - 1][e->y]->getHeight() == 1) {
				path.push(board[e->x - 1][e->y]);
			} else if (board[e->x][e->y - 1]->getHeight() == 1) {
				path.push(board[e->x][e->y - 1]);
			}
			break;
		case 3:
			if (board[e->x][e->y - 1]->getHeight() == 1) {
				path.push(board[e->x][e->y - 1]);
			} else if (board[e->x + 1][e->y]->getHeight() == 1) {
				path.push(board[e->x + 1][e->y]);
			} else if (board[e->x][e->y + 1]->getHeight() == 1) {
				path.push(board[e->x][e->y + 1]);
			} else if (board[e->x - 1][e->y]->getHeight() == 1) {
				path.push(board[e->x - 1][e->y]);
			}
			break;
	}
	path.push(e);
	return path;
}

std::stack<Tile *> findPath(Tile *start, Tile *end) {
	std::map<Tile *, int> open;
	std::vector<Tile *> closed;
	std::map<Tile *, Tile *> cameFrom;
	Tile *current;
	open[start] = 0;
	while (current != end) {
		int min = open.begin()->second;
		current = open.begin()->first;
		std::map<Tile *, int>::iterator it;
		for (it = open.begin(); it != open.end(); ++it) {
			if (it->second < min) {
				min = it->second;
				current = it->first;
			}
		}
		open.erase(current);
		closed.push_back(current);
		for (int i = -1; i <= 1; i++) {
			Tile *neighbour = board[current->x + i][current->y];
			if (std::find(closed.begin(), closed.end(), neighbour) ==
				closed.end() && neighbour->getHeight() == 1) {
				int tentative = abs(neighbour->x - start->x) +
								abs(neighbour->y - start->y) +
								abs(neighbour->x - end->x) +
								abs(neighbour->y - end->y);
				if (open.count(neighbour) == 0) {
					open[neighbour] = tentative;
					cameFrom[neighbour] = current;
				}
				if (tentative < open[neighbour]) {
					open[neighbour] = tentative;
					cameFrom[neighbour] = current;
				}
			}
		}
		for (int i = -1; i <= 1; i++) {
			Tile *neighbour = board[current->x][current->y + i];
			if (std::find(closed.begin(), closed.end(),neighbour) ==
				closed.end() && neighbour->getHeight() == 1) {
				int tentative = abs(neighbour->x - start->x) +
								abs(neighbour->y - start->y) +
								abs(neighbour->x - end->x) +
								abs(neighbour->y - end->y);
				if (open.count(neighbour) == 0) {
					open[neighbour] = tentative;
					cameFrom[neighbour] = current;
				}
				if (tentative < open[neighbour]) {
					open[neighbour] = tentative;
					cameFrom[neighbour] = current;
				}
			}
		}
	}
	std::stack<Tile *> path;
	while (current != start) {
		current = cameFrom[current];
		path.push(current);
	}
	return path;
}
