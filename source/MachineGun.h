#ifndef __MACHINEGUN_H__
#define __MACHINEGUN_H__
#include "GunComponent.h"
#include "HitscanComponent.h"
#include <utility>

class MachineGun : public GunComponent {
private:
	HitscanComponent *ray;
public:
	MachineGun(PhysicalComponent *,PhysicalComponent *);
	void stop();
	void update(std::pair<int,int>);
};

#endif
