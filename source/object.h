#ifndef __OBJECT_H_
#define __OBJECT_H_
#include <SDL2/SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <string>

class Object {
	protected:
		std::string path;
		SDL_Texture *image;
		SDL_Rect *source;
		SDL_Rect *inGame;
		SDL_Rect *onScreen;
		float angle;
		int height;
		void initialize();
		void declarations();
		void setScreenPos();
		void updateHeight(int);	
		void pointTo(Object&);	

	public:
		static SDL_Renderer *renderer;
		Object();
		Object(std::string);
		~Object();
		int getHeight();
		int alignment;
		int damage;
		void rotate(float);
		void rotatePoint(float, Object&);
		void updatePath(std::string);
		void updateInGame(int, int, int=-1, int=-1);
		void updateOnScreen(int, int, int=-1, int=-1);
		void updateSource(int, int, int=-1, int=-1);
		void moveToDest(Object&);
		void moveToDestCenter(Object&);
		void move(int, int);
		virtual void render();
		virtual bool collision(Object&);
		bool inside(Object&);

	friend class Display;
};

#endif
