#ifndef __ENEMY_H__
#define __ENEMY_H__
#include "character.h"

class Enemy : public Character {
	private:
		bool hit;
		Object *health;

	public:
		Enemy(int);
		bool onTarget();
		bool isHit();
		bool collision(Object &o);
		void render();
		void stopMoving();
		bool overheated();
};

#endif
