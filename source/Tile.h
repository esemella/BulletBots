#ifndef __TILE_H__
#define __TILE_H__
#include "GameObject.h"

class Tile : public GameObject {
private:
	bool open;
	bool border;
public:
	Tile(PhysicalComponent*,bool,bool);
	bool isOpen();
	bool isBorder();
	void update();
	PhysicalComponent *spawnObject();
};

#endif 
