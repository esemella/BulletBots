#ifndef __GUNCOMPONENT_H__
#define __GUNCOMPONENT_H__
#include "GameObject.h"
#include "PhysicalComponent.h"
#include "DamageComponent.h"
#include "DirectionComponent.h"
#include "Timer.h"
#include <utility>

class GunComponent {
protected:
	PhysicalComponent *space;
	PhysicalComponent *target;
	GraphicalComponent *image;
	DamageComponent *damage;
	DirectionComponent *direction;
	bool activated;
	bool overheat;
	int heat;
	int maxHeat;
	int addHeat;
	Timer *fireRate;
	Timer *coolDown;
	Timer *delay;
public:
	GunComponent(PhysicalComponent *,PhysicalComponent *);
	void shoot();
	void heatUp();
	float getHeat();
	void changeTarget(PhysicalComponent*);
	virtual void stop();
	virtual void update(std::pair<int,int>);
};

#endif
