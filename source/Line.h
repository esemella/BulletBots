#ifndef __LINE_H__
#define __LINE_H__
#include <SDL2/SDL.h>
//#include <SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include "GraphicalComponent.h"

class Line : public GraphicalComponent {
private:
	int r,g,b,a;
	int x1,y1,x2,y2;
public:
	Line(int,int,int,int=255);
	void updateBounds(int=-1,int=-1,int=-1,int=-1);
	bool addToScreen(int);
	void render();
};

#endif
