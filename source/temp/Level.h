#ifndef __LEVEL_H__
#define __LEVEL_H__
#include "Enemy.h"
#include "Player.h"
#include "DamageComponent.h"
#include <vector>

class Level {
private:
	Level();
	static Level *instance;
	static bool instanceFlag;
	std::vector<Enemy *> enemies;
	std::vector<AnimationComponent *> explosions;
	Player *player;
public:
	~Level();
	static Level *getInstance();
	void populate();
	void update();
	bool collision(PhysicalComponent &, DamageComponent *);
	bool isEnemyOccupied(PhysicalComponent&, Character*);
};

#endif
