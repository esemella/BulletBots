#include "GraphicalComponent.h"
#include "Screen.h"
#include <iostream>

static SDL_Renderer *renderer;
int count = 0;

GraphicalComponent::GraphicalComponent(std::string path): path(path) {	
	bounds = NULL;
	source = NULL;
	point = NULL;
	angle = 0;
	a = 0;
}

GraphicalComponent::~GraphicalComponent() {
	texture = NULL;
	delete bounds;
	delete source;
	delete point;
}

void GraphicalComponent::initialize() {
	SDL_Surface *loadsurface = IMG_Load(path.c_str());
	SDL_SetColorKey(loadsurface, SDL_TRUE, SDL_MapRGB(loadsurface->format, 0x00, 0xFF, 0xFF));
	texture = SDL_CreateTextureFromSurface(renderer, loadsurface);
	SDL_FreeSurface(loadsurface);
	Screen::getInstance()->addToDict(path, texture);
}

void GraphicalComponent::updateBounds(int x, int y, int w, int h) {
	if (!bounds) {
		bounds = new SDL_Rect;
	}
	Screen *screen = Screen::getInstance();
	if (x >= 0 && y >= 0) {
		std::pair<int, int> position = screen->getPosition();
		bounds->x = x - position.first;
		bounds->y = y - position.second;
	}
	if (w >= 0 && h >= 0) {
		bounds->w = w;
		bounds->h = h;
	}
}

void GraphicalComponent::updateSource(int x, int y, int w, int h) {
	if (!source) {
		source = new SDL_Rect;
	}
	if (x >= 0 && y >= 0) {
		source->x = x;
		source->y = y;
	}
	if (w >= 0 && h >= 0) {
		source->w = w;
		source->h = h;
	}
}

bool GraphicalComponent::addToScreen(int index) {
	Screen *screen = Screen::getInstance();
	if (screen->isOnScreen(bounds->x, bounds->y) ||
		screen->isOnScreen(bounds->x + bounds->w, bounds->y) ||
		screen->isOnScreen(bounds->x, bounds->y + bounds->h) ||
		screen->isOnScreen(bounds->x + bounds->w, bounds->y + bounds->h)) {
		screen->addToScreen(this, index);
		return true;
	}
	return false;
}

void GraphicalComponent::rotate(float rotation) {
	if (rotation < 0) {
		angle = rotation + 360;
	} else if (rotation > 360) {
		angle = rotation - 360;
	} else {
		angle = rotation;
	}
}

void GraphicalComponent::rotatePoint(int x, int y) {
	if (!point) {
		point = new SDL_Point;
	}
	point->x = x;
	point->y = y;
}

void GraphicalComponent::setAlpha(float ratio) {
	texture = Screen::getInstance()->getTexture(path);
	if (!texture) {
		initialize();
	}
	int temp = ratio * 255;
	a = temp;
	SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
	SDL_SetTextureAlphaMod(texture, a);
}

void GraphicalComponent::render() {
	texture = Screen::getInstance()->getTexture(path);
	if (!texture) {
		initialize();
	}
	if ((!source) && (!angle)) {
		SDL_RenderCopyEx(renderer, texture, NULL, bounds, 0, NULL, SDL_FLIP_NONE);
	} else if (!angle) {
		SDL_RenderCopyEx(renderer, texture, source, bounds, 0, NULL, SDL_FLIP_NONE);
	} else if (!source) {
		if (point) {
			SDL_RenderCopyEx(renderer, texture, NULL, bounds, angle, point, SDL_FLIP_NONE);
		} else {
			SDL_RenderCopyEx(renderer, texture, NULL, bounds, angle, NULL, SDL_FLIP_NONE);
		}
	} else {
		if (point) {
			SDL_RenderCopyEx(renderer, texture, source, bounds, angle, point, SDL_FLIP_NONE);
		} else {
			SDL_RenderCopyEx(renderer, texture, source, bounds, angle, NULL, SDL_FLIP_NONE);	
		}
	}
}
