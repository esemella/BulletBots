#include "Tile.h"
#include <string>

const std::string srcFloor = "Assets/Tiles/Floor.png";
const std::string srcWall = "Assets/Tiles/Wall.png";
const std::string srcBlock = "Assets/Tiles/Block.png";

Tile::Tile(PhysicalComponent *object, bool border, bool open):
GameObject(), border(border), open(open) {
	space = object;
	if (border) {
		image = new GraphicalComponent(srcWall);
	} else if (open) {
		image = new GraphicalComponent(srcFloor);
	} else {
		image = new GraphicalComponent(srcBlock);
	}
	std::pair<int, int> position = space->getPosition();
	image->updateBounds(position.first, position.second, 64, 64);
}

bool Tile::isOpen() {
	return open;
}

bool Tile::isBorder() {
	return border;
}

void Tile::update() {
	std::pair<int, int> position = space->getPosition();
	image->updateBounds(position.first, position.second);
	if (open) {
		image->addToScreen(0);
	} else {
		image->addToScreen(7);
	}
}

PhysicalComponent *Tile::spawnObject() {
	PhysicalComponent *temp = new PhysicalComponent(*space);
	return temp;
}
