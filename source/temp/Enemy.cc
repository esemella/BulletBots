#include "Enemy.h"
#include "Player.h"
#include "World.h"

const int speed = 256;
const int size = 54;
const int imageSize = 64;
const int offset = 5;
const std::string srcEnemy = "Assets/Enemy/EnemyTorso.png";


Enemy::Enemy(PhysicalComponent *spawnPoint): NonPlayerCharacter() {
	std::pair<int,int> center = spawnPoint->getCenter();
	space = new PhysicalComponent(center.first - (size / 2),
								center.second - (size / 2),
								size, size);
	control = new ControlComponent(space, speed);
	image = new GraphicalComponent(srcEnemy);
	std::pair<int, int> position = space->getPosition();
	image->updateBounds(position.first, position.second, imageSize, imageSize);
	image->updateSource(5, 10, 80, 60);
	direction = new DirectionComponent(space);
	rotate = new RotateComponent(space, size / 2);
	int r = rand() % 3;
	switch (r) {
		case 0: rightGun = new MachineGun(space, Player::getInstance()->getSpace()); break;
		case 1: rightGun = new Rifle(space, Player::getInstance()->getSpace()); break;
		case 2: rightGun = new ShotGun(space, Player::getInstance()->getSpace()); break;
	}
	r = rand() % 3;
	switch (r) {
		case 0: leftGun = new MachineGun(space, Player::getInstance()->getSpace()); break;
		case 1: leftGun = new Rifle(space, Player::getInstance()->getSpace()); break;
		case 2: leftGun = new ShotGun(space, Player::getInstance()->getSpace()); break;
	}
	PhysicalComponent temp = PhysicalComponent(*space);
	temp.changePosition(center.first, center.second + size);
	changeTarget(World::getInstance()->getPresentTile(&temp)->getSpace());
	dash = new DashComponent(space, control);
	shield = new ShieldComponent();
	status = new StatusComponent(60);
	AI = new AIComponent(this);
}

void Enemy::update() {
	AI->getInput();
	control->applyMovement();
	std::pair<int,int> position = space->getPosition();
	image->updateBounds(position.first - offset, position.second - offset);
	image->rotate(direction->getDirection());
	image->addToScreen(4);
	Character::update();
}
