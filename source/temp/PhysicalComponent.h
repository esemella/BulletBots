#ifndef __PHYSICALCOMPONENT_H__
#define __PHYSICALCOMPONENT_H__
#include <utility>

class PhysicalComponent {
protected:
	int posX, posY, width, height;
public:
	PhysicalComponent(int,int,int,int);
	PhysicalComponent(const PhysicalComponent&);
	std::pair<int, int> getPosition();
	std::pair<int, int> getCenter();
	std::pair<int, int> getDifference(PhysicalComponent&);
	void changePosition(int,int);
	void changeSize(int,int);
	bool collision(PhysicalComponent&);
	bool inside(PhysicalComponent*);
};

#endif
