#ifndef __GRAPHICALCOMPONENT_H__
#define __GRAPHICALCOMPONENT_H__
#include <SDL2/SDL.h>
//#include <SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <string>

class GraphicalComponent {
private:
	SDL_Rect *bounds;
	SDL_Rect *source;
	SDL_Texture *texture;
	SDL_Point *point;
	float angle;
	std::string path;
	void initialize();
	Uint8 a;
public:
	static SDL_Renderer *renderer;
	GraphicalComponent(std::string);
	~GraphicalComponent();
	void updateSource(int=-1,int=-1,int=-1,int=-1);
	bool isOnScreen();
	void rotate(float);
	void rotatePoint(int,int);
	void setAlpha(float);
	virtual void updateBounds(int=-1,int=-1,int=-1,int=-1);
	virtual bool addToScreen(int);
	virtual void render();
};

#endif
