#ifndef __GAMEOBJECT_H__
#define __GAMEOBJECT_H__
#include "PhysicalComponent.h"
#include "GraphicalComponent.h"
#include <utility>

class GameObject {
protected:
	PhysicalComponent *space;
	GraphicalComponent *image;
public:
	GameObject();
	~GameObject();
	PhysicalComponent *getSpace();
};

#endif
