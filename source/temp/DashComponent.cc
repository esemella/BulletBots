#include "DashComponent.h"
#include "World.h"
#include <iostream>

const int speed = 1024;
extern Timer *timestep;

DashComponent::DashComponent(PhysicalComponent *space, ControlComponent *control):
space(space), control(control), activated(false), dirX(1), dirY(1) {
	dashTime = new Timer();
	cooldown = new Timer();
	cooldown->start();
}

void DashComponent::activate() {
	float time = cooldown->getTime() / 1000.f;
	if (time < 0.7) {
		return;	
	}
	activated = true;
	std::pair<int,int> vel = control->getVelocity();
	control->disable(true);
	if (vel.first > 0 ) {
		dirX = 1;
	} else if (vel.first < 0) {
		dirX = -1;
	} else {
		dirX = 0;
	}
	if (vel.second > 0) {
		dirY = 1;
	} else if (vel.second < 0) {
		dirY = -1;
	} else {
		dirY = 0;
	}
	dashTime->start();
}

void DashComponent::update() {
	if (!activated) {
		return;
	}
	float time = dashTime->getTime() / 1000.f;
	float step = timestep->getTime() / 1000.f;
	std::pair<int,int> position = space->getPosition();
	PhysicalComponent temp(*space);
	temp.changePosition(position.first + speed * step * dirX,
						position.second + speed * step * dirY);
	if (!World::getInstance()->collision(temp) && time < 0.1) {
		position.first += (speed * step * dirX);
		position.second += (speed * step * dirY);
		space->changePosition(position.first, position.second);
	} else {
		activated = false;
		control->disable(false);
		cooldown->start();
	}
}
