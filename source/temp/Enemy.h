#ifndef __ENEMY_H__
#define __ENEMY_H__
#include "NonPlayerCharacter.h"
#include "MachineGun.h"
#include "ShotGun.h"
#include "GunComponent.h"
#include "ShieldComponent.h"
#include "StatusComponent.h"
#include "AIComponent.h"

class Enemy : public NonPlayerCharacter {
private:
	AIComponent *AI;
public:
	Enemy(PhysicalComponent *);
	void update();
};

#endif
