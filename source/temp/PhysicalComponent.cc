#include "PhysicalComponent.h"
#include <cmath>
#include <iostream>

PhysicalComponent::PhysicalComponent(int posX, int posY, int width, int height):
posX(posX), posY(posY), width(width), height(height) {}

PhysicalComponent::PhysicalComponent(const PhysicalComponent &p):
posX(p.posX), posY(p.posY), width(p.width), height(p.height) {}

std::pair<int, int> PhysicalComponent::getPosition() {
	return std::make_pair(posX, posY);
}

std::pair<int, int> PhysicalComponent::getCenter() {
	return std::make_pair(posX + (width / 2), posY + (height / 2));
}

std::pair<int, int> PhysicalComponent::getDifference(PhysicalComponent &p) {
	return std::make_pair(posX - p.posX, posY - p.posY);
}

void PhysicalComponent::changePosition(int x, int y) {
	posX = x;
	posY = y;
}

void PhysicalComponent::changeSize(int w, int h) {
	width = w;
	height = h;
}

bool PhysicalComponent::collision(PhysicalComponent &obj) {
	return posX + width >= obj.posX &&
			posX <= obj.posX + obj.width &&
			posY + height >= obj.posY &&
			posY <= obj.posY + obj.height;
}

bool PhysicalComponent::inside(PhysicalComponent *obj) {
	return posX >= obj->posX && posY >= obj->posY &&
			posX + width <= obj->posX + obj->width &&
			posY + height <= obj->posY + obj->height;
}
