#ifndef __CONTROLCOMPONENT_H__
#define __CONTROLCOMPONENT_H__
#include "PhysicalComponent.h"
#include <utility>

class ControlComponent {
private:
	bool disabled;
	int speed, diagSpeed, velX, velY;
	PhysicalComponent *space;
public:
	ControlComponent(PhysicalComponent *,int);
	void applyMovement();
	void changeVelocity(int,int);
	void disable(bool);
	std::pair<int,int> getVelocity();
};

#endif
