#ifndef __Control_H__
#define __Control_H__
#include <vector>
#include <set>
#include <stack>
#include <map>
#include "character.h"
#include "display.h"
#include "enemy.h"

class Control {
	private:
		static Control *instance;
		static bool instanceFlag;
		std::set<Enemy *> shooting;
		std::set<Enemy *> moving;
		std::vector<Character *> targets;
		std::map<Enemy *, std::stack<Tile *> > instruction;
		void shoot(Enemy*);

	public:
		~Control();
		static Control *getInstance();
		void update(std::vector<Object *>*);
		void getControlled(std::vector<Object *>*);
		void addControlled(Enemy *);
		void addTarget(Character *);
		bool activated(Enemy *);
		void remove(Enemy *);
};

bool lineOfSight(Tile&, Tile&);
std::pair<int, int> getDirection(Tile&, Tile&);
std::stack<Tile *> findPath(Tile*, Tile*);
std::stack<Tile *> getFreeSpace(Tile*);

#endif
