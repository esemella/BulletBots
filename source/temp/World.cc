#include "World.h"
#include "Screen.h"
#include "Player.h"
#include "Level.h"
#include <utility>
#include <iostream>

extern int screenWidth;
extern int screenHeight;
const int gridWidth = 10240;
const int gridHeight = 6144;
const int borderSize = 1280;
int tileSize = 64;

bool World::instanceFlag = false;
World *World::instance = NULL;

World *World::getInstance() {
	if (!instanceFlag) {
		instance = new World();
		instanceFlag = true;
	}
	return instance;
}

World::World() {
	grid = new Tile**[(gridWidth + (borderSize * 2)) / tileSize];
	for (int i = 0; i < (gridWidth + (borderSize * 2)) / tileSize; i++) {
		grid[i] = new Tile*[(gridHeight + (borderSize * 2)) / tileSize];
		for (int j = 0; j < (gridHeight + (borderSize * 2)) / tileSize; j++) {
			grid[i][j] = NULL;
			createTile(i, j, true);
		}
	}
	int type = rand() % 3;
	int x = 0;
	int y = 0;
	int direction = -1;
	while(!generateRoom(x, y, direction)) {
		x = rand() % (gridWidth / tileSize - 30) + (borderSize / tileSize + 30);
		y = rand() % (gridHeight / tileSize - 30) + (borderSize / tileSize + 30);
		direction = rand() % 4;
	}
	Player *player = Player::getInstance(getSpawnPoint());
	for (int i = 0; i < (gridWidth + borderSize * 2) / tileSize; i++) {
		for (int j = 0; j < (gridHeight + borderSize * 2) / tileSize; j++) {
			Screen *screen = Screen::getInstance();
			std::pair<int, int> positionScreen = screen->getPosition();
			std::pair<int, int> position = grid[i][j]->getSpace()->getPosition();
			if (!grid[i][j]->isBorder() && grid[i][j]->isOpen() &&
	 			!screen->isOnScreen(position.first - positionScreen.first,
									position.second - positionScreen.second)) {
				int r = rand() % 20;
				if (r == 0) {
					spawnPoints.push_back(grid[i][j]);
				}
			}
		}
	}
}

World::~World() {
	instanceFlag = false;
	for (int i = 0; i < (gridWidth + (borderSize * 2)) / tileSize; i++) {
		for (int j = 0; j < (gridHeight + (borderSize * 2)) / tileSize; j++) {
			delete grid[i][j];
		}
		delete [] grid[i];
	}
	delete [] grid;
}

bool World::generateRoom(int x, int y, int direction) {
	int w = rand() % 20 + 8;
	int h = rand() % 20 + 8;
	int startX = 0;
	int endX = 0;
	int startY = 0;
	int endY = 0;
	if (direction == 0) {
		startX = x - (w / 2);
		if (w % 2 == 1) {
			endX = x + (w / 2) + 1;
		} else {
			endX = x + (w / 2);
		}
		startY = y - h;
		endY = y;
	} else if (direction == 1) {
		startX = x;
		endX = x + w;
		startY = y - (h / 2);
		if (h % 2 == 1) {
			endY = y + (h / 2) + 1;
		} else {
			endY = y + (h / 2);
		}
	} else if (direction == 2) {
		startX = x - (w / 2);
		if (w % 2 == 1) {
			endX = x + (w / 2) + 1;
		} else {
			endX = x + (w / 2);
		}
		startY = y;
		endY = y + h;
	} else if (direction == 3) {
		startX = x - w;
		endX = x;
		startY = y - (h / 2);
		if (h % 2 == 1) {
			endY = y + (h / 2) + 1;
		} else {
			endY = y + (h / 2);
		}
	}

	if (!checkSpace(startX, startY, endX - startX, endY - startY)) {
		return false;
	}

	for (int i = startX; i < endX; i++) {
		for (int j = startY; j < endY; j++) {
			createTile(i, j, false);
			if (spawnPoints.empty() && !grid[i][j]->isBorder() &&
				grid[i][j]->isOpen()) {
				int r = rand() % 15;
				if (r == 0) {
					spawnPoints.push_back(grid[i][j]);
				}
			}
		}
	}

	for (int i = 0; i < 4; i++) {
		if (direction != i) {
			if (i == 0) {
				generateTunnel(startX + (w / 2), startY, 0);
			} else if (i == 1) {
				generateTunnel(startX + w, startY + (h / 2), 1);
			} else if (i == 2) {
				generateTunnel(startX + (w / 2), startY + h, 2);
			} else if (i == 3) {
				generateTunnel(startX, startY + (h / 2), 3);
			}
		}
	}
	return true;
}


void World::generateTunnel(int x, int y, int direction) {
	int h = rand() % 15 + 3;
	int w = 4;
	if (direction == 0 && generateRoom(x + w - 2, y - h, direction) &&
		checkSpace(x, y, w - 2, -h)) {
		for (int i = x - 2; i < x + w - 2; i++) {
			for (int j = y; j >= y - h; j--) {
				createTile(i, j, false);
			}
		}
	} else if (direction == 1 && generateRoom(x + h, y + w - 2, direction) &&
				checkSpace(x, y, h, w - 2)) {
		for (int i = x; i < x + h; i++) {
			for (int j = y - 2; j < y + w - 2; j++) {
				createTile(i, j, false);
			}
		}
	} else if (direction == 2 && generateRoom(x + w - 2, y + h, direction) &&
				checkSpace(x, y, w - 2, h)) {
		for (int i = x - 2; i < x + w - 2; i++) {
			for (int j = y; j < y + h; j++) {
				createTile(i, j, false);
			}
		}
	} else if (direction == 3 && generateRoom(x - h, y + w - 2, direction) &&
				checkSpace(x, y, -h, w - 2)) {
		for (int i = x; i >= x - h; i--) {
			for (int j = y - 2; j < y + w - 2; j++) {
				createTile(i, j, false);
			}
		}
	}
}

bool World::checkSpace(int x, int y, int w, int h) {
	if (x + w >= (gridWidth + borderSize) / tileSize || x < borderSize / tileSize ||
		y + h >= (gridHeight + borderSize) / tileSize || y < borderSize / tileSize) {
		return false;
	}
	for (int i = x; i < x + w; i++) {
		for (int j = y; j < y + h; j++) {
			if (!grid[i][j]->isBorder()) {
				return false;
			}
		}
	}
	return true;
}

void World::createTile(int x, int y, bool border) {
	delete grid[x][y];
	PhysicalComponent *space = new PhysicalComponent(x * tileSize, y * tileSize,
													tileSize, tileSize);
	int r = rand() % 5;
	if (border) {
		grid[x][y] = new Tile(space, border, false);
	} else {
		bool closed = true;
		if (r == 0) {
			for (int i = x - 2; i <= x + 2; i++) {
				if ((!grid[i][y - 2]->isOpen() && !grid[i][y - 2]->isBorder()) ||
					(!grid[i][y + 2]->isOpen() && !grid[i][y + 2]->isBorder())) {
					closed = false;
				}
			}
			for (int i = y - 2; i <= y + 2; i++) {
				if ((!grid[x - 2][i]->isOpen() && !grid[x - 2][i]->isBorder()) ||
					(!grid[x + 2][i]->isOpen() && !grid[x + 2][i]->isBorder())) {
					closed = false;
				}
			}
		} else {
			closed = false;
		}
		grid[x][y] = new Tile(space, border, !closed);
	}
}

void World::update() {
	std::pair<int, int> position = Screen::getInstance()->getPosition();
	for (int i = position.first / tileSize; i < (position.first + screenWidth) / tileSize + 1; i++) {
		for (int j = position.second / tileSize; j < (position.second + screenHeight) / tileSize + 1; j++) {
			grid[i][j]->update();
		}
	}
}

bool World::collision(PhysicalComponent &obj) {
	std::pair<int, int> position = obj.getPosition();
	int centerX = position.first / tileSize;
	int centerY = position.second / tileSize;
	for (int i = centerX - 2; i < centerX + 2; i++) {
		for (int j = centerY - 2; j < centerY + 2; j++) {
			if (!grid[i][j]->isOpen() && grid[i][j]->getSpace()->collision(obj)) {
				return true;
			}
		}
	}
	return false;
}

PhysicalComponent *World::getSpawnPoint() {
	PhysicalComponent *temp = spawnPoints[0]->spawnObject();
	spawnPoints.erase(spawnPoints.begin());	
	return temp;
}

bool World::availableSpawn() {
	return !spawnPoints.empty();
}

Tile *World::getPresentTile(PhysicalComponent *space) {
	std::pair<int,int> position = space->getPosition();
	int x = position.first / tileSize;
	int y = position.second / tileSize;
	return grid[x][y];
}

int World::getTileSize() {
	return tileSize;
}
