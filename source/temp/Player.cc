#include "Player.h"
#include "Screen.h"
#include "Reticle.h"
#include "Level.h"
#include <utility>
#include <iostream>

bool Player::instanceFlag = false;
Player *Player::instance = NULL;

const int speed = 256;
const int size = 56;
const int imageSize = 64;
const int offset = 4;
const std::string srcPlayer = "Assets/Player/PlayerTorso.png";

Player *Player::getInstance(PhysicalComponent *spawnPoint) {
	if (!instanceFlag && spawnPoint) {
		instance = new Player(spawnPoint);
		instanceFlag = true;
	}
	return instance;
}

Player::Player(PhysicalComponent *spawnPoint): Character() {
	space = new PhysicalComponent(*spawnPoint);
	space->changeSize(size, size);
	control = new ControlComponent(space, speed);
	image = new GraphicalComponent(srcPlayer);
	Screen *screen = Screen::getInstance();
	std::pair<int, int> position = space->getPosition();
	screen->updateScreen(position.first, position.second);
	image->updateBounds(position.first, position.second, imageSize, imageSize);
	image->updateSource(0, 0, 60, 60);
	direction = new DirectionComponent(space);
	direction->changeTarget(Reticle::getInstance()->getSpace());
	rotate = new RotateComponent(space, size / 2);
	int r = 0;//rand() % 3;
	switch (r) {
		case 0: rightGun = new MachineGun(space, Reticle::getInstance()->getSpace()); break;
		case 1: rightGun = new Rifle(space, Reticle::getInstance()->getSpace()); break;
		case 2: rightGun = new ShotGun(space, Reticle::getInstance()->getSpace()); break;
	}
	r = 0;//rand() % 3;
	switch (r) {
		case 0: leftGun = new MachineGun(space, Reticle::getInstance()->getSpace()); break;
		case 1: leftGun = new Rifle(space, Reticle::getInstance()->getSpace()); break;
		case 2: leftGun = new ShotGun(space, Reticle::getInstance()->getSpace()); break;
	}
	dash = new DashComponent(space, control);
	shield = new ShieldComponent();
	status = new StatusComponent(100);
	Reticle::getInstance()->addRightGun(rightGun);
	Reticle::getInstance()->addLeftGun(leftGun);
}

Player::~Player() {
	instanceFlag = false;
}

void Player::update() {
	control->applyMovement();
	std::pair<int, int> position = space->getPosition();
	Screen *screen = Screen::getInstance();
	screen->updateScreen(position.first, position.second);
	image->updateBounds(position.first - offset, position.second - offset);
	image->rotate(direction->getDirection());
	image->addToScreen(4);
	Character::update();
}
