#include "StatusComponent.h"
#include <iostream>

StatusComponent::StatusComponent(int maxShieldCap):
maxShieldCap(maxShieldCap), shieldCap(maxShieldCap) {
	shieldRecharge = 1;
	clock = new Timer();
	clock->start();
}

void StatusComponent::takeDamage(DamageComponent *damage) {
	shieldCap -= damage->getDamage();
}

void StatusComponent::update() {
	float time = clock->getTime() / 1000.f;
	if (time >= 1) {
		shieldCap += shieldRecharge;
		if (shieldCap > maxShieldCap) {
			shieldCap = maxShieldCap;
		}
		clock->start();
	}
}

bool StatusComponent::fullShield() {
	return shieldCap == maxShieldCap;
}

bool StatusComponent::isDead() {
	return shieldCap <= 0;
}
