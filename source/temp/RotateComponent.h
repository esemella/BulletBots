#ifndef __ROTATECOMPONENT_H__
#define __ROTATECOMPONENT_H__
#include "PhysicalComponent.h"
#include <utility>

class RotateComponent {
private:
	PhysicalComponent *space;
	PhysicalComponent *target;
	int offset;
public:
	RotateComponent(PhysicalComponent *, int offset);
	void changeTarget(PhysicalComponent*);
	std::pair<int,int> getRightArm();
	std::pair<int,int> getLeftArm();
};

#endif
