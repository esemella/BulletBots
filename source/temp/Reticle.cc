#include "Reticle.h"
#include "Screen.h"
#include "Player.h"

bool Reticle::instanceFlag = false;
Reticle *Reticle::instance = NULL;
const std::string srcReticle = "Assets/HUD/Reticle.png";
const std::string srcRightBar = "Assets/HUD/rightBar.png";
const std::string srcLeftBar = "Assets/HUD/leftBar.png";
const int size = 10;
const int offset = 5;

Reticle *Reticle::getInstance() {
	if (!instanceFlag) {
		instance = new Reticle();
		instanceFlag = true;
	}
	return instance;
}

Reticle::Reticle() {
	space = new PhysicalComponent(0, 0, 0, 0);

	image = new GraphicalComponent(srcReticle);
	image->updateBounds(0, 0, size, size);

	rightBar = new GraphicalComponent(srcRightBar);
	rightBar->updateBounds(0, 0, size * 2, size * 3);
	rightGun = NULL;
	leftBar = new GraphicalComponent(srcLeftBar);
	leftBar->updateBounds(0, 0, size * 2, size * 3);
	leftGun = NULL;
}

Reticle::~Reticle() {
	instanceFlag = false;
}

void Reticle::processInput(int x, int y) {
	if (x >= 0 && y >= 0) {
		std::pair<int, int> position = Screen::getInstance()->getPosition();
		space->changePosition(x + position.first, y + position.second);
		std::pair<int, int> positionDiff = space->getDifference(*(Player::getInstance()->getSpace()));
		playerX = positionDiff.first;
		playerY = positionDiff.second;
	}
}

void Reticle::addRightGun(GunComponent *g) {
	rightGun = g;
}

void Reticle::addLeftGun(GunComponent *g) {
	leftGun = g;
}

void Reticle::update() {
	std::pair<int, int> positionP = Player::getInstance()->getSpace()->getPosition();
	space->changePosition(positionP.first + playerX, positionP.second + playerY);
	std::pair<int, int> position = space->getPosition();
	image->updateBounds(position.first - offset, position.second - offset);
	image->addToScreen(8);
	if (rightGun) {
		rightBar->setAlpha(rightGun->getHeat());
	}
	rightBar->updateBounds(position.first - offset + 10, position.second - offset - 10);
	rightBar->addToScreen(8);
	if (leftGun) {
		leftBar->setAlpha(leftGun->getHeat());
	}
	leftBar->updateBounds(position.first - offset - 20, position.second - offset - 10);
	leftBar->addToScreen(8);
}
