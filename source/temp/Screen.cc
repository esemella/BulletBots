#include "Screen.h"
#include <iostream>

static SDL_Renderer *renderer;
bool Screen::instanceFlag = false;
Screen *Screen::instance = NULL;

Screen *Screen::getInstance(int w, int h) {
	if (!instanceFlag) {
		instance = new Screen(w, h);
		instanceFlag = true;
	}
	return instance;
}

Screen::Screen(int w, int h) {
	if (w >= 0 && h >= 0) {
		renderArea = new SDL_Rect;
		renderArea->w = w;
		renderArea->h = h;
		renderArea->x = 0;
		renderArea->y = 0;
	}
	for (int i = 0; i < 9; i++) {
		images.push_back(std::stack<GraphicalComponent *>());
	}
}

Screen::~Screen() {
	instanceFlag = false;
	delete renderArea;
}

void Screen::updateScreen(int x, int y) {
	renderArea->x = x - (renderArea->w / 2);
	renderArea->y = y - (renderArea->h / 2);
}

bool Screen::isOnScreen(int x, int y) {
	return x < renderArea->w && x >= 0 && y < renderArea->h && y >= 0;
}

void Screen::addToScreen(GraphicalComponent *image, int index) {
	images[index].push(image);
}

std::pair<int, int> Screen::getPosition() {
	return std::make_pair(renderArea->x, renderArea->y);
}

void Screen::addToDict(std::string path, SDL_Texture *t) {
	textureDict[path] = t;
}

SDL_Texture *Screen::getTexture(std::string path) {
	if (textureDict.find(path) == textureDict.end() ) {
	  return NULL;
	}
	return textureDict[path];
}
	
void Screen::render() {
	SDL_SetRenderTarget(renderer, NULL);
	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(renderer);
	for (int i = 0; i < 9; i++) {
		while (!images[i].empty()) {
			images[i].top()->render();
			images[i].pop();
		}
	}
	SDL_RenderPresent(renderer);
	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(renderer);	
}
