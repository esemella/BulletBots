#ifndef __CHARACTER_H__
#define __CHARACTER_H__
#include "GameObject.h"
#include "DirectionComponent.h"
#include "ControlComponent.h"
#include "GunComponent.h"
#include "Rifle.h"
#include "ShotGun.h"
#include "MachineGun.h"
#include "DashComponent.h"
#include "ShieldComponent.h"
#include "RotateComponent.h"
#include "StatusComponent.h"
#include "DamageComponent.h"
#include "AnimationComponent.h"

class Character : public GameObject {
protected:
	Character();
	DirectionComponent *direction;
	ControlComponent *control;
	GunComponent *leftGun;
	GunComponent *rightGun;
	DashComponent *dash;
	ShieldComponent *shield;
	RotateComponent *rotate;
	StatusComponent *status;
public:
	void processInput(int,int);
	void changeTarget(PhysicalComponent*);
	void activateSpecial();
	void shootLeft();
	void stopLeft();
	void shootRight();
	void stopRight();
	void takeDamage(DamageComponent *);
	bool isDead();
	virtual void update();
};

#endif
