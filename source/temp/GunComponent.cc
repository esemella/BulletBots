#include "GunComponent.h"
#include <iostream>

GunComponent::GunComponent(PhysicalComponent *space, PhysicalComponent *target):
space(space), target(target), activated(false), heat(0), maxHeat(40), overheat(false) {
	fireRate = new Timer();
	fireRate->start();
	coolDown = new Timer();
	coolDown->start();
	direction = new DirectionComponent(space);
}

void GunComponent::changeTarget(PhysicalComponent *target) {
	direction->changeTarget(target);
}

void GunComponent::shoot() {
	if (!overheat) {
		activated = true;
	}
}

void GunComponent::update(std::pair<int,int> offset) {
	std::pair<int,int> pos = space->getPosition();
	image->updateBounds(pos.first + offset.first - 25, pos.second + offset.second - 25);
	direction->changeOffset(offset);
	image->rotate(direction->getDirection());
	image->addToScreen(3);
	float time = coolDown->getTime() / 1000.f;
	if (!activated && time > 1) {
		heat -= 10;
		coolDown->start();
	} else if (overheat && time > 0.5) {
		heat -= 5;
		coolDown->start();
	}
	if (heat < 1) {
		overheat = false;
		heat = 1;
	}
}

void GunComponent::heatUp() {
	heat += addHeat;
	if (heat > maxHeat) {
		heat = maxHeat;
		overheat = true;
		stop();
	}
}

float GunComponent::getHeat() {
	float h = heat;
	float m = maxHeat;
	return h / m;
}

void GunComponent::stop() {}
