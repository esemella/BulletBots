#ifndef __SHIELDCOMPONENT_H__
#define __SHIELDCOMPONENT_H__
#include "GraphicalComponent.h"
#include "PhysicalComponent.h"
#include "Timer.h"
#include "StatusComponent.h"

class ShieldComponent {
private:
	GraphicalComponent *image;
	bool activated;
	Timer *shieldTime;
	int angle;
public:
	ShieldComponent();
	void activate();
	void update(PhysicalComponent &, StatusComponent &);
};

#endif
