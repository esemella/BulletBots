#include "DirectionComponent.h"
#include <cmath>
#include <utility>
#include <iostream>

const float pi = 3.14159265358;

DirectionComponent::DirectionComponent(PhysicalComponent *space, int x, int y):
target(NULL), space(space), offset(std::make_pair(x, y)) {
	target = NULL;
}

void DirectionComponent::changeTarget(PhysicalComponent *t) {
	target = t;
}

void DirectionComponent::changeOffset(std::pair<int,int> o) {
	offset = o;
}

float DirectionComponent::getDirection() {
	if (!target) {
		return 0;
	}
	std::pair<float, float> posTarget = target->getCenter();
	std::pair<float, float> posSpace = space->getCenter();
	if (offset.first != 0 && offset.second != 0) {
		posSpace = space->getPosition();
		posSpace.first += offset.first;
		posSpace.second += offset.second;
	}
	if (posTarget.second > posSpace.second) {
		return 180 + (atan((posTarget.first - posSpace.first) /
					(posSpace.second - posTarget.second)) * 180 / pi);
	} else if (posTarget.second < posSpace.second) {
		return -1 * (atan((posSpace.first - posTarget.first) /
					(posSpace.second - posTarget.second)) * 180 / pi);
	}
	return 0;
}
