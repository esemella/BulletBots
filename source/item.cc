#include "item.h"

const std::string srcItem = "Assets/Textures/Item.png";

Item::Item(): Object(srcItem) {
	updateHeight(1);
	updateInGame(0, 0, 20, 20);
}
