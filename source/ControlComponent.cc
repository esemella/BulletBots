#include "ControlComponent.h"
#include "Timer.h"
#include "World.h"
#include "Level.h"
#include <iostream>

extern Timer *timestep;

ControlComponent::ControlComponent(PhysicalComponent *space, int speed):
space(space), speed(speed), velX(0), velY(0), disabled(false) {
	diagSpeed = sqrt((speed * speed) / 2);
}

void ControlComponent::applyMovement() {
	if (disabled) {
		return;
	}
	std::pair<int,int> position = space->getPosition();
	float time = timestep->getTime() / 1000.f;
	PhysicalComponent temp(*space);
	temp.changePosition(position.first + velX * time,
						position.second + velY * time);
	if (!World::getInstance()->collision(temp)) {
		position.first += (velX * time);
		position.second += (velY * time);
	}
	space->changePosition(position.first, position.second);
}

void ControlComponent::changeVelocity(int x, int y) {
	if (x != 0 && y != 0) {
		velX += x * diagSpeed;
		velY += y * diagSpeed;
	} else {
		velX += x * speed;
		velY += y * speed;
	}
}

std::pair<int,int> ControlComponent::getVelocity() {
	return std::make_pair(velX, velY);
}

void ControlComponent::disable(bool d) {
	disabled = d;
}
