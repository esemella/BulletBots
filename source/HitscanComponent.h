#ifndef __HITSCANCOMPONENT_H__
#define __HITSCANCOMPONENT_H__
#include "PhysicalComponent.h"
#include "GraphicalComponent.h"
#include "DamageComponent.h"
#include "Line.h"
#include "Timer.h"
#include <vector>
#include <utility>

class HitscanComponent {
private:
	std::vector<GraphicalComponent *> bullets;
	float angle;
	Timer *displayTime;
public:
	HitscanComponent();
	void shoot(PhysicalComponent*,PhysicalComponent*,std::pair<int,int>, DamageComponent *);
	bool display();
};

#endif
