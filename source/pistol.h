#ifndef __PISTOL_H__
#define __PISTOL_H__
#include <vector>
#include <string>
#include "arm.h"
#include "object.h"
#include "bullet.h"
#include "timer.h"

class Pistol : public Arm {
	private:
		int damage;
		int alignment;
		std::vector<Bullet *> bullets;
		Timer *fireRate;
		bool ready;

	public:
		Pistol(int);
		~Pistol();
		void shoot(Object&, Object&);
		void checkCollision(Object&);
		void reset();
		void update();
		void render();
		bool empty();
};

#endif
