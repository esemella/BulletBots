#ifndef __DASHCOMPONENT_H__
#define __DASHCOMPONENT_H__
#include "PhysicalComponent.h"
#include "ControlComponent.h"
#include "Timer.h"

class DashComponent {
private:
	PhysicalComponent *space;
	ControlComponent *control;
	bool activated;
	int dirX, dirY;
	Timer *cooldown;
	Timer *dashTime;
public:
	DashComponent(PhysicalComponent *,ControlComponent *);
	void activate();
	void update();
};

#endif
