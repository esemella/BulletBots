#include "RotateComponent.h"
#include <cmath>

const int radius = 25;

RotateComponent::RotateComponent(PhysicalComponent *space, int offset):
space(space),/* target(NULL),*/ offset(offset) {}

void RotateComponent::changeTarget(PhysicalComponent *t) {
	target = t;
}

std::pair<int,int> RotateComponent::getRightArm() {
	if (!target) {
		return std::make_pair(0, 0);	
	}
	std::pair<float,float> posSpace = space->getCenter();
	std::pair<float,float> posTarget = target->getCenter();
	float angle;
	if ((posTarget.first <= posSpace.first && posTarget.second <= posSpace.second) ||
		(posTarget.first >= posSpace.first && posTarget.second >= posSpace.second)) {
		angle = atan2(std::abs(posTarget.second - posSpace.second),
					std::abs(posTarget.first - posSpace.first)) + 3.14 / 2;
	} else {
		angle = atan2(std::abs(posTarget.second - posSpace.second),
					std::abs(posTarget.first - posSpace.first)) -  3.14 / 2;
	}
	float adj = cos(angle) * radius;
	float opp = sin(angle) * radius;
	if (posTarget.first <= posSpace.first) {
		adj *= -1;
	}
	if (posTarget.second <= posSpace.second) {
		opp *= -1;
	}
	return std::make_pair(adj + offset, opp + offset);
}

std::pair<int,int> RotateComponent::getLeftArm() {
	if (!target) {
		return std::make_pair(0, 0);
	}
	std::pair<float,float> posSpace = space->getCenter();
	std::pair<float,float> posTarget = target->getCenter();
	float angle;
	if ((posTarget.first <= posSpace.first && posTarget.second <= posSpace.second) ||
		(posTarget.first >= posSpace.first && posTarget.second >= posSpace.second)) {
		angle = atan2(std::abs(posTarget.second - posSpace.second),
					std::abs(posTarget.first - posSpace.first)) - 3.14 / 2;
	} else {
		angle = atan2(std::abs(posTarget.second - posSpace.second),
					std::abs(posTarget.first - posSpace.first)) +  3.14 / 2;
	}
	float adj = cos(angle) * radius;
	float opp = sin(angle) * radius;
	if (posTarget.first <= posSpace.first) {
		adj *= -1;
	}
	if (posTarget.second <= posSpace.second) {
		opp *= -1;
	}
	return std::make_pair(adj + offset, opp + offset);
}
