#ifndef __TILE_H__
#define __TILE_H__
#include <string>
#include <utility>
#include <stack>
#include "object.h"

class Tile : public Object {
	private:
		bool open;
		bool border;

	public:
		Tile(std::string, int, int, int, bool, bool);
		bool isOpen();
		bool isBorder();
		int x, y;
};

#endif 
