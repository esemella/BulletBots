#ifndef __BULLET_H__
#define __BULLET_H__
#include "object.h"
#include "tile.h"

class Bullet : public Object {
	private:
		int targetX, targetY;
		float velX, velY;
		bool targetReached;

	public:
		Bullet();
		Bullet(Object&, Object&, int);
		void applyMovement();
		void update();
		Tile* getPresentTile();
};

#endif
