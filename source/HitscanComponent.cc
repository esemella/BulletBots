#include "HitscanComponent.h"
#include "World.h"
#include "Reticle.h"
#include "Level.h"
#include <utility>
#include <cmath>
#include <iostream>

const int size = 2;
const int maxCount = 1280;

HitscanComponent::HitscanComponent() {
	GraphicalComponent *b = new Line(255, 255, 183);
	b->updateBounds(0, 0, 0, 0);
	b->addToScreen(2);
	bullets.push_back(b);
	for (int i = 0; i < 4; i++) {
		GraphicalComponent *bullet = new Line(255, 255, 183, 180);
		bullet->updateBounds(0, 0, 0, 0);
		bullet->addToScreen(2);
		bullets.push_back(bullet);
	}
	displayTime = new Timer();
	angle = 0;
}

void HitscanComponent::shoot(PhysicalComponent *space, PhysicalComponent *target, std::pair<int,int> offset, DamageComponent *damage) {
	std::pair<float,float> posSpace = space->getPosition();
	std::pair<float,float> posTarget = target->getPosition();
	float velX = posSpace.first + offset.first;
	float velY = posSpace.second + offset.second;
	angle =  atan2(std::abs(posTarget.second - velY),
						std::abs(posTarget.first - velX));
	float stepX = cos(angle);
	float stepY = sin(angle);
	if (posTarget.first <= velX) {
		stepX *= -1;
	}
	if (posTarget.second <= velY) {
		stepY *= -1;
	}
	PhysicalComponent temp = PhysicalComponent(posSpace.first, posSpace.second, 1, 1);
	while (!World::getInstance()->collision(temp)) {
		if (!space->collision(temp) && Level::getInstance()->collision(temp,damage)) {
			break;
		}
		velX += stepX;
		velY += stepY;
		temp.changePosition(velX, velY);
	}
	bullets[0]->updateBounds(posSpace.first + offset.first, posSpace.second + offset.second,
						velX, velY);
	bullets[0]->addToScreen(2);
	bullets[1]->updateBounds(posSpace.first + offset.first - 1, posSpace.second + offset.second,
						velX - 1, velY);
	bullets[1]->addToScreen(2);
	bullets[2]->updateBounds(posSpace.first + offset.first + 1, posSpace.second + offset.second,
						velX + 1, velY);
	bullets[2]->addToScreen(2);
	bullets[3]->updateBounds(posSpace.first + offset.first, posSpace.second + offset.second - 1,
						velX, velY - 1);
	bullets[3]->addToScreen(2);
	bullets[4]->updateBounds(posSpace.first + offset.first, posSpace.second + offset.second + 1,
						velX, velY + 1);
	bullets[4]->addToScreen(2);
	displayTime->start();
}

bool HitscanComponent::display() {
	float time = displayTime->getTime() / 1000.f;
	if (time < 0.02) {
		return true;
	}
	return false;
}
