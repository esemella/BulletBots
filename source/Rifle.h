#ifndef __RIFLE_H__
#define __RIFLE_H__
#include "GunComponent.h"
#include "HitscanComponent.h"
#include <utility>

class Rifle : public GunComponent {
private:
	std::vector<HitscanComponent *> rays;
public:
	Rifle(PhysicalComponent *, PhysicalComponent *);
	void update(std::pair<int,int>);
};

#endif
