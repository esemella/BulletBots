#ifndef __TIMER_H__
#define __TIMER_H__
#include <SDL2/SDL.h>
//#include <SDL_ttf.h>
#include <SDL2/SDL_image.h>

class Timer {
private:
	Uint32 timeStart;
public:
	Timer();
	Uint32 getTime();
	void start();
};

#endif
