#ifndef __LEVEL_H__
#define __LEVEL_H__
#include <SDL2/SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <vector>
#include <cstdlib>
#include <string>
#include "character.h"
#include "arm.h"
#include "object.h"
#include "tile.h"
#include "timer.h"
#include "input.h"
#include "display.h"
#include "control.h"
#include "item.h"
#include "player.h"
#include "enemy.h"

class Level {
	private:
		int seed;
		Player *player;
		std::vector<Enemy *> enemies;
		std::vector<Arm *> items;
		std::vector<Object *> onScreen;
		std::vector<Object *> spawnPoints;
		bool checkSpace(int, int, int, int);
		bool generateRoom(int, int, int);
		void generateTunnel(int, int, int);
		void createTile(int, int, bool);

	public:
		static SDL_Renderer *renderer;
		Level(int);
		~Level();
		void loop();
};

#endif
