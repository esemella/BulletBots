#include "display.h"

int screenWidth = 0;
int screenHeight = 0;
const int barHeight = 680;
const int barLength = 30;
const int barWidth = 277;
const std::string source1 = "Assets/Textures/Reticle.png";
const std::string source2 = "Assets/Textures/ShieldBar.png";
const std::string source3 = "Assets/Textures/OverheatBar.png";

bool Display::instanceFlag = false;
Display *Display::instance = NULL;

Display *Display::getInstance(int a, int b) {
	if (!instanceFlag && a != 0 && b != 0) {
		instance = new Display(a, b);
		instanceFlag = true;
	}
	return instance;
}

Display::Display(int w, int h) {
	cursor = new Object(source1);
	cursor->updateInGame(0, 0, 10, 10);
	cursor->updateOnScreen(0, 0, 10, 10);
	cursor->updateHeight(5);
	screen = new Object();
	screen->updateInGame(0, 0, w, h);
	screen->updateHeight(2);
	screenWidth = w;
	screenHeight = h;
	health = new Object(source2);
	health->updateOnScreen(501, barHeight, barWidth, barLength);
	primary = new Object(source3);
	primary->updateOnScreen(890, barHeight, 0, 0);
	secondary = new Object(source3);
	secondary->updateOnScreen(112, barHeight, 0, 0);
}

Display::~Display() {
	instanceFlag = false;
}

void Display::setCursor(int x, int y) {
	if (x >= 0 && x <= screenWidth - 10 &&
		y >= 0 && y <= screenHeight - 10) {
		cursor->updateOnScreen(x, y);
		cursor->moveToDest(*screen);
		cursor->move(x, y);
	}
}

void Display::render() {
	cursor->render();
	health->render();
	primary->render();
	secondary->render();
}

void Display::update(Character &player) {
	screen->moveToDestCenter(player);
	screen->move(-screenWidth / 2, -screenHeight / 2);
	lowerX = screen->inGame->x;
	lowerY = screen->inGame->y;
	upperX = screen->inGame->x + screen->inGame->w;
	upperY = screen->inGame->y + screen->inGame->h;
	cursor->moveToDest(*screen);
	cursor->move(cursor->onScreen->x, cursor->onScreen->y);
}

Object *Display::getScreen() {
	return screen;
}

Object *Display::getCursor() {
	return cursor;
}

void Display::updateShield(float ratio) {
	health->updateOnScreen(501, barHeight, barWidth * ratio, barLength);
}

void Display::updatePrimary(float ratio) {
	primary->updateOnScreen(112, barHeight, barWidth * ratio, barLength);
}

void Display::updateSecondary(float ratio) {
	secondary->updateOnScreen(890, barHeight, barWidth * ratio, barLength);
}
