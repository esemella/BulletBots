#include "Character.h"
#include <iostream>

Character::Character(): GameObject() {}

void Character::processInput(int x, int y) {
	control->changeVelocity(x, y);
}

void Character::changeTarget(PhysicalComponent *target) {
	direction->changeTarget(target);
	rotate->changeTarget(target);
	leftGun->changeTarget(target);
	rightGun->changeTarget(target);
}

void Character::shootLeft() {
	leftGun->shoot();
}

void Character::stopLeft() {
	leftGun->stop();
}

void Character::shootRight() {
	rightGun->shoot();
}

void Character::stopRight() {
	rightGun->stop();
}

void Character::activateSpecial() {
	dash->activate();
}

void Character::takeDamage(DamageComponent *damage) {
	status->takeDamage(damage);
	shield->activate();
}

bool Character::isDead() {
	return status->isDead();
}

void Character::update() {
	status->update();
	leftGun->update(rotate->getLeftArm());
	rightGun->update(rotate->getRightArm());
	shield->update(*space, *status);
	dash->update();
}
