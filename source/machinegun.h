#ifndef __MACHINEGUN_H__
#define __MACHINEGUN_H__
#include <vector>
#include <string>
#include "arm.h"
#include "object.h"
#include "bullet.h"
#include "timer.h"

class Machinegun : public Arm {
	private:
		int damage;
		int alignment;
		std::vector<Bullet *> bullets;
		Timer *fireRate;

	public:
		Machinegun(int);
		~Machinegun();
		void shoot(Object&, Object&);
		void checkCollision(Object&);
		void reset();
		void update();
		void render();
		bool empty();
};

#endif
