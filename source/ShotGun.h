#ifndef __SHOTGUN_H__
#define __SHOTGUN_H__
#include "GunComponent.h"
#include "HitscanComponent.h"
#include <vector>
#include <utility>

class ShotGun : public GunComponent {
private:
	std::vector<HitscanComponent *> rays;
public:
	ShotGun(PhysicalComponent *, PhysicalComponent *);
	void update(std::pair<int,int>);
};

#endif
