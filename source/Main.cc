#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
//#include <SDL_ttf.h>
#include <ctime>
#include <iostream>
#include "Timer.h"
#include "Screen.h"
#include "World.h"
#include "Input.h"
#include "Player.h"
#include "Level.h"
#include "Reticle.h"
#include "GraphicalComponent.h"

int screenWidth;
int screenHeight;
SDL_Window *window = NULL;
SDL_Renderer *renderer = NULL;
SDL_Renderer *GraphicalComponent::renderer;
SDL_Renderer *Screen::renderer;
Timer *timestep;

int main(int, char**) {
	screenWidth = 1280;
	screenHeight = 768;
	SDL_Init(SDL_INIT_VIDEO);
	window = SDL_CreateWindow("Bullet Bots", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenWidth, screenHeight - 10, SDL_WINDOW_SHOWN);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	SDL_SetRenderTarget(renderer, NULL);	
	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	GraphicalComponent::renderer = renderer;
	Screen::renderer = renderer;
	int img = IMG_INIT_PNG;
	IMG_Init(img);		
	SDL_ShowCursor(SDL_DISABLE);

	int seed = time(NULL);
//	int seed = 1474846406;

std::cout << seed << std::endl;

	srand(seed);
	Screen *screen = Screen::getInstance(screenWidth, screenHeight);
	World *world = World::getInstance();
	Input *input = Input::getInstance();
	Player *player = Player::getInstance();
	Reticle *reticle = Reticle::getInstance();
	Level *level = Level::getInstance();

	timestep = new Timer();
	timestep->start();

Timer *t = new Timer();
t->start();
int frames = 0;

	while (input->active()) {
		frames++;
		input->process();
		reticle->update();
		player->update();
		level->update();
		world->update();
		timestep->start();
		screen->render();
	}

std::cout <<  frames / (t->getTime() / 1000.f) << std::endl;

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	IMG_Quit();
//	TTF_Quit();
	SDL_Quit();
}
