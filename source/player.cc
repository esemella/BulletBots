#include "player.h"
#include "display.h"

const std::string srcPlayer = "Assets/Textures/Animations/Spritesheets/Walking/ForwardWalkAnimation.png";

Player::Player(int size): Character(srcPlayer, size, 1) {
	shieldCapacity = 40;
	shieldMaxCapacity = 40;
	shooting.push_back(false);
	Arm *m = new Pistol(alignment);
	gun.push_back(m);
	shooting.push_back(false);
	Arm *s = new Shotgun(alignment);
	gun.push_back(s);
	grabArm = 0;

}

void Player::update(std::vector<Object *> *canCollide) {
	Character::update(canCollide);	
	for (int i = 0; i < gun.size(); i++) {
		Display *d = Display::getInstance();
		float ratio = gun[i]->getRatio();
		if (i == 0) {
			d->updatePrimary(ratio);
		} else {
			d->updateSecondary(ratio);
		}
	}
	Display *d = Display::getInstance();
	float x = shieldCapacity;
	float y = shieldMaxCapacity;
	float ratio = x / y;
	if (shieldCapacity >= shieldMaxCapacity) {
		ratio = 0;
	}
	d->updateShield(ratio);
}

bool Player::collision(Object &o) {
	bool collide = Character::collision(o);
	Display *d = Display::getInstance();
	float x = shieldCapacity;
	float y = shieldMaxCapacity;
	float ratio = x / y;
	d->updateShield(ratio);
	if (shieldCapacity <= 0 || shieldCapacity >= shieldMaxCapacity) {
		d->updateShield(0);
	}
	return collide;
}

void Player::switchPrimary() {
	grabArm = 1;	
}

void Player::switchSecondary() {
	grabArm = 2;
}

Arm *Player::pickUp(Arm *a) {
	if (grabArm == 0) {
		return NULL;
	}
	Arm *retVal;
	if (grabArm == 1) {
		retVal = gun[0];
		gun[0] = a;
	} else if (grabArm == 2) {
		retVal = gun[1];
		gun[1] = a;
	}
	grabArm = 0;
	return retVal;
}
