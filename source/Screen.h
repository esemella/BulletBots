#ifndef __SCREEN_H__
#define __SCREEN_H__
#include "GraphicalComponent.h"
#include <SDL2/SDL.h>
//#include <SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <vector>
#include <string>
#include <utility>
#include <stack>
#include <map>

class Screen {
private:
	Screen(int,int);
	static Screen *instance;
	static bool instanceFlag;
	SDL_Rect *renderArea;
	std::vector<std::stack<GraphicalComponent*> > images;
	std::map<std::string, SDL_Texture *> textureDict;
public:
	~Screen();
	static Screen *getInstance(int=-1,int=-1);
	static SDL_Renderer *renderer;
	void updateScreen(int,int);
	void addToScreen(GraphicalComponent*,int);
	bool isOnScreen(int,int);
	std::pair<int,int> getPosition();
	void addToDict(std::string,SDL_Texture *);
	SDL_Texture *getTexture(std::string);
	void render();
};

#endif
