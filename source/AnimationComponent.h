#ifndef __ANIMATIONCOMPONENT_H__
#define __ANIMATIONCOMPONENT_H__
#include "GraphicalComponent.h"
#include "PhysicalComponent.h"
#include "Timer.h"
#include <string>
#include <utility>

class AnimationComponent {
private:
	GraphicalComponent *sequence;
	Timer *timer;
	int posX, posY;
	bool done;
	int frame;
public:
	AnimationComponent(PhysicalComponent*);
	void update();
	bool isDone();
};

#endif
