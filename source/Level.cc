#include "Level.h"
#include "World.h"
#include "Player.h"
#include <iostream>

bool Level::instanceFlag = false;
Level *Level::instance = NULL;

Level *Level::getInstance() {
	if (!instanceFlag) {
		instance = new Level();
		instanceFlag = true;
	}
	return instance;
}

Level::Level() {
	populate();
}

void Level::populate() {
	World *world = World::getInstance();
	while (world->availableSpawn() > 0) {
		Enemy *e = new Enemy(world->getSpawnPoint());	
		enemies.push_back(e);
	}
	player = Player::getInstance();
}

void Level::update() {
	std::vector<Enemy *>::iterator it;
	for (it = enemies.begin(); it < enemies.end(); ++it) {
		if ((*it)->isDead()) {
			PhysicalComponent *space = new PhysicalComponent(*(*it)->getSpace());
			AnimationComponent *e = new AnimationComponent(space);
			explosions.push_back(e);
			enemies.erase(it);
		} else {
			(*it)->update();
		}
	}
	std::vector<AnimationComponent *>::iterator it2;
	for (it2 = explosions.begin(); it2 < explosions.end(); ++it2) {
		if (!(*it2)->isDone()) {
			(*it2)->update();
		} else {
			explosions.erase(it2);
		}
	}
}

bool Level::collision(PhysicalComponent &space, DamageComponent *damage) {
	if (player->getSpace()->collision(space)) {
		player->takeDamage(damage);
		return true;
	}
	std::vector<Enemy *>::iterator it;
	for (it = enemies.begin(); it < enemies.end(); ++it) {
		if ((*it)->getSpace()->collision(space)) {
			(*it)->takeDamage(damage);
			return true;
		}
	}
	return false;
}

bool Level::isEnemyOccupied(PhysicalComponent &space, Character *subject) {
	std::vector<Enemy *>::iterator it;
	for (it = enemies.begin(); it < enemies.end(); ++it) {
		if ((*it)->getSpace()->collision(space) && *it != subject) {
			return true;
		}
	}
	return false;
}
