#include "timer.h"

Timer::Timer() {
	timeStart = 0;
}

Uint32 Timer::getTime() {
	return SDL_GetTicks() - timeStart;
}

void Timer::start() {
	timeStart = SDL_GetTicks();
}
