#ifndef __INPUT_H__
#define __INPUT_H__
#include <SDL2/SDL.h>
#include "timer.h"
#include "character.h"
#include "display.h"
#include "player.h"

class Input {
	private:
		SDL_Event e;
		bool quit;
		bool dragging;
		Input();
		Player *player;
		static Input *instance;
		static bool instanceFlag;

	public:
		~Input();
		bool active();
		void update();
		void addPlayer(Player *);
		static Input *getInstance();
};

#endif
