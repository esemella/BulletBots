#ifndef __WORLD_H__
#define __WORLD_H__
#include "Tile.h"
#include "PhysicalComponent.h"
#include <vector>

class World {
private:
	World();
	static World *instance;
	static bool instanceFlag;
	Tile*** grid;
	std::vector<Tile *> spawnPoints;
	bool checkSpace(int, int, int, int);
	bool generateRoom(int, int, int);
	void generateTunnel(int, int, int);
	void createTile(int, int, bool);
public:
	~World();
	static World *getInstance();
	PhysicalComponent *getSpawnPoint();
	Tile *getPresentTile(PhysicalComponent *);
	bool availableSpawn();
	bool collision(PhysicalComponent&);
	int getTileSize();
	void update();
};

#endif
