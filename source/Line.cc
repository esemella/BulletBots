#include "Line.h"
#include "Screen.h"

Line::Line(int r, int g, int b, int a): GraphicalComponent(""),
r(r), g(g), b(b), a(a), x1(0), y1(0), x2(0), y2(0) {}

void Line::updateBounds(int x, int y, int i, int j) {
	Screen *screen = Screen::getInstance();
	std::pair<int,int> position = screen->getPosition();
	if (x >= 0 && y >= 0) {
		x1 = x - position.first;
		y1 = y - position.second;
	}
	if (i >= 0 && j >= 0) {
		x2 = i - position.first;
		y2 = j - position.second;
	}
}

bool Line::addToScreen(int index) {
	Screen *screen = Screen::getInstance();
	if (screen->isOnScreen(x1, y1) ||
		screen->isOnScreen(x2, y2)) {
		screen->addToScreen(this, index);
		return true;
	}
	return false;
}

void Line::render() {
	if (a != 255) {
		SDL_SetRenderDrawBlendMode(GraphicalComponent::renderer, SDL_BLENDMODE_BLEND);
	} else {
		SDL_SetRenderDrawBlendMode(GraphicalComponent::renderer, SDL_BLENDMODE_NONE);
	}
	SDL_SetRenderDrawColor(GraphicalComponent::renderer, r, g, b, a);
	SDL_RenderDrawLine(GraphicalComponent::renderer, x1, y1, x2, y2);
}
