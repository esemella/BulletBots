#include "shotgun.h"
#include "display.h"

extern Tile ***board;
const std::string srcBullet = "Assets/Textures/Bullet.png";

Shotgun::Shotgun(int a): Arm() {
	damage = 5;
	alignment = a;
	fireRate = new Timer();
	ready = true;
}

Shotgun::~Shotgun() {
	for (int i = 0; i < bullets.size(); i++) {
		delete bullets[i];
		bullets[i] = NULL;
	}
}

void Shotgun::shoot(Object &s, Object &t) {
	Object o;
	o.updateInGame(0, 0, 0, 0);
	o.moveToDest(t);
	float time = fireRate->getTime() / 1000.f;	
	if ((time == 0 || time > 0.6) && ready && !disabled) {
		Bullet *b = new Bullet(s, o, damage);
		b->updatePath(srcBullet);
		bullets.push_back(b);
		b = NULL;
		for (int i = -2; i <= 2; i++) {
			if (i == 0) {
				continue;
			}
			Object a;
			a.updateInGame(0, 0, 0, 0);
			a.moveToDest(t);
			a.rotatePoint(0.07 * i, s);
			b = new Bullet(s, a, damage);
			b->updatePath(srcBullet);
			bullets.push_back(b);
		}
		fireRate->start();
		ready = false;
		heatCapacity += 5;
		heatTime->start();
	}
	Arm::update();	
}

void Shotgun::reset() {
	ready = true;
}

void Shotgun::update() {
	Arm::update();
	std::vector<Bullet *>::iterator it;
	for (it = bullets.begin(); it < bullets.end(); ++it) {
		(*it)->update();
		(*it)->applyMovement();
		Tile *t = (*it)->getPresentTile();
		for (int i = t->x - 1; i <= t->x + 1; i++) {
			for (int j = t->y - 1; j <= t->y + 1; j++) {
				checkCollision(*board[i][j]);
			}
		}
	}
}

void Shotgun::checkCollision(Object &obstacle) {
	Display *d = Display::getInstance();
	std::vector<Bullet *>::iterator it;
	for (it = bullets.begin(); it < bullets.end(); ++it) {
		if (((*it)->collision(obstacle) && obstacle.getHeight() == 2 &&
			obstacle.alignment != alignment) || !d->getScreen()->collision(**it)) {
			obstacle.collision(**it);
			bullets.erase(it);
			break;
		}
	}
}

void Shotgun::render() {
	Display *d = Display::getInstance();
	std::vector<Bullet *>::iterator it;
	for (it = bullets.begin(); it < bullets.end(); ++it) {
		if (d->getScreen()->collision(**it)) {
			(*it)->render();
		}
	}
}

bool Shotgun::empty() {
	return bullets.empty();
}
