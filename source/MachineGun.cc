#include "MachineGun.h"
#include "Character.h"
#include <utility>
#include <string>

const std::string src = "Assets/Guns/Gattling.png";

MachineGun::MachineGun(PhysicalComponent *space, PhysicalComponent *target):
GunComponent(space, target) {
	ray = new HitscanComponent();
	damage = new DamageComponent(4);
	image = new GraphicalComponent(src);
	std::pair<int,int> pos = space->getPosition();
	image->updateBounds(pos.first, pos.second, 50, 50);
	addHeat = 1;
}

void MachineGun::stop() {
	activated = false;
}

void MachineGun::update(std::pair<int,int> offset) {
	GunComponent::update(offset);
	if (activated && !ray->display()) {
		float time = fireRate->getTime() / 1000.f;
		if (time == 0 || time > 0.075) {
			int tempX = -15 + (rand() % 30);
			int tempY = -15 + (rand() % 30);
			std::pair<int,int> position = target->getPosition();
			PhysicalComponent *temp = new PhysicalComponent(*target);
			temp->changePosition(tempX + position.first, tempY + position.second);
			fireRate->start();
			ray->shoot(space, temp, offset, damage);
			heatUp();
		}
	}
}
