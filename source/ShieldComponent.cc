#include "ShieldComponent.h"
#include <utility>

const std::string srcShield = "Assets/Enemy/Shield.png";
const int size = 72;
const int offset = 8;

ShieldComponent::ShieldComponent() {
	image = new GraphicalComponent(srcShield);
	image->updateBounds(0, 0, size, size);
	shieldTime = new Timer();
	activated = false;
	angle = 0;
}

void ShieldComponent::activate() {
	activated = true;
	shieldTime->start();
	angle = 0;
}

void ShieldComponent::update(PhysicalComponent &space, StatusComponent &status) {
	float time = shieldTime->getTime() / 1000.f;
	if (status.fullShield()) {
		activated = false;
	}
	if (time < 3 && activated) {
		std::pair<int,int> pos = space.getPosition();
		image->updateBounds(pos.first - offset, pos.second - offset);
		angle += 30;
		image->rotate(angle);
		image->addToScreen(5);
	}
}
