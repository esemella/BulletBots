#include "arm.h"
#include "display.h"

Arm::Arm() {
	heatMaxCapacity = 20;
	heatTime = new Timer();
	heatTime->start();
	disabled = false;
	dropped = NULL;
}

void Arm::update() {
	float time = heatTime->getTime() / 1000.f;
	if (disabled && time >= 5.5) {
		disabled = false;
		heatCapacity = 0;
	} else if (heatCapacity >= heatMaxCapacity) {
		disabled = true;
	} else if (heatCapacity > 0 && time >= 0.5 && !disabled) {
		heatCapacity -= 1;
		heatTime->start();
	}
	if (heatCapacity < 0) {
		heatCapacity = 0;
	}
}

float Arm::getRatio() {
	float x = heatCapacity;
	float y = heatMaxCapacity;
	float ratio =  x / y;
	return ratio;
}

void Arm::drop(Object &location) {
	dropped = new Item();
	dropped->moveToDest(location);
	dropped->move(20, 20);
}

void Arm::grabbed() {
	delete dropped;
	dropped = NULL;
}

Item *Arm::getDropped() {
	if (dropped) {
		return dropped;
	}
	return NULL;
}

bool Arm::overheated() {
	return disabled;
}
