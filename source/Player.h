#ifndef __PLAYER_H__
#define __PLAYER_H__
#include "Character.h"

class Player : public Character {
private:
	Player(PhysicalComponent *);
	static Player *instance;
	static bool instanceFlag;
public:
	~Player();
	static Player *getInstance(PhysicalComponent* =NULL);
	void update();
};

#endif
