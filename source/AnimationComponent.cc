#include "AnimationComponent.h"
#include <string>
#include <iostream>

const std::string src = "Assets/Enemy/Explosion.png";

AnimationComponent::AnimationComponent(PhysicalComponent *space) {
	sequence = new GraphicalComponent(src);
	frame = 0;
	std::pair<int,int> pos = space->getCenter();
	posX = pos.first;
	posY = pos.second;
	sequence->updateBounds(posX - 45, posY - 45, 90, 90);
	sequence->addToScreen(6);
	timer = new Timer();
	timer->start();
	done = false;
}

void AnimationComponent::update() {
	if (timer->getTime() == 0) {
		return;
	}
	float time = timer->getTime() / 1000.f;
	if (time >= 0.03) {
		frame++;
		if (frame > 7) {
			done = true;
		}
		timer->start();
	}
	if (sequence) {
		sequence->updateBounds(posX - 45, posY - 45);
		sequence->addToScreen(6);
	}
}

bool AnimationComponent::isDone() {
	return done;
}
