#include "AIComponent.h"
#include "World.h"
#include "Player.h"
#include "Screen.h"
#include "Level.h"
#include <utility>
#include <iostream>
#include <algorithm>

extern int screenWidth;
extern int screenHeight;
World *world;
Player *player;
Screen *screen;
Level *level;
const int range = 400;
const int offset = 300;

AIComponent::AIComponent(Character *c): subject(c), state(inactive) {
	world = World::getInstance();
	player = Player::getInstance();
	screen = Screen::getInstance();
	level = NULL;
}

void AIComponent::getInput() {
	switch (state) {
	case inactive:
		if (onScreen() && lineOfSight(player->getSpace())) {
			Tile *start = world->getPresentTile(subject->getSpace());
			Tile *end = world->getPresentTile(player->getSpace());
			instructions = findPath(start, end);
			instructions.pop();
			target = instructions.top()->getSpace();
			instructions.pop();
			state = searching;	
			subject->changeTarget(player->getSpace());	
			moveToTarget();
		}
		return;
	case shooting:
		if (onScreen() && inRange()) {
/*
			if (lineOfSight(player->getSpace())) {
				subject->shootRight();
			} else {
				state = inactive;
			}
*/
			if (clearShot(player->getSpace())) {
				subject->shootRight();
			} else {
				Tile *current = world->getPresentTile(subject->getSpace());
				Tile *next = getFreeTile(current);
				target = next->getSpace();
				state = clearing;
				moveToTarget();
			}
		} else {
			subject->stopRight();
			Tile *start = world->getPresentTile(subject->getSpace());
			Tile *end = world->getPresentTile(player->getSpace());
			instructions = findPath(start, end);
			instructions.pop();
			target = instructions.top()->getSpace();
			instructions.pop();
			state = searching;
			moveToTarget();
		}
		return;
	case searching:
		if (inCenter() && lineOfSight(player->getSpace())) {
			subject->processInput(lastInput.first * -1, lastInput.second * -1);
			subject->changeTarget(player->getSpace());
			state = shooting;
		} else if (inCenter() && subject->getSpace()->inside(target)) {
			subject->processInput(lastInput.first * -1, lastInput.second * -1);
			if (!instructions.empty()) {
				target = instructions.top()->getSpace();
				instructions.pop();
				moveToTarget();
			} else {
				Tile *start = world->getPresentTile(subject->getSpace());
				Tile *end = world->getPresentTile(player->getSpace());
				instructions = findPath(start, end);
				instructions.pop();
				target = instructions.top()->getSpace();
				instructions.pop();
				moveToTarget();
			}
		}
		return;
	case clearing:
		if (inCenter() && subject->getSpace()->inside(target)) {
			subject->processInput(lastInput.first * -1, lastInput.second * -1);
			subject->changeTarget(player->getSpace());
			state = shooting;
		}
		return;
	}
}

bool AIComponent::onScreen() {
	std::pair<int,int> pos = screen->getPosition();
	PhysicalComponent temp = PhysicalComponent(pos.first, pos.second, screenWidth,
							screenHeight);
	return subject->getSpace()->collision(temp);
}

bool AIComponent::inRange() {
	std::pair<int,int> subjectPos = subject->getSpace()->getCenter();
	std::pair<int,int> targetPos = player->getSpace()->getCenter();
	float opp = std::abs(subjectPos.second - targetPos.second);
	float adj = std::abs(subjectPos.first - targetPos.first);
	float hypotenuse = sqrt(pow(opp, 2) + pow(adj, 2));
	int r = (rand() % offset) - offset / 2;
	return hypotenuse <= range + r;
}

bool AIComponent::inCenter() {
	Tile *closest = world->getPresentTile(subject->getSpace());
	std::pair<int,int> tileCenter = closest->getSpace()->getCenter();
	std::pair<int,int> subCenter = subject->getSpace()->getCenter();
	int diffX = std::abs(tileCenter.first - subCenter.first);
	int diffY = std::abs(tileCenter.second - subCenter.second);
	return diffX < 3 && diffY < 3;
}

bool AIComponent::lineOfSight(PhysicalComponent *p) {
	std::pair<int,int> start = subject->getSpace()->getCenter();
	std::pair<int,int> end = p->getCenter();
	int dx = abs(end.first - start.first);
	int dy = abs(end.second - start.second);
	int x = start.first;
	int y = start.second;
	int n = 1 + dx + dy;
	int xInc = (end.first > start.first) ? 1 : -1;
	int yInc = (end.second > start.second) ? 1 : -1;
	int error = dx - dy;
	dx *= 2;
	dy *= 2;
	for (; n > 0; --n) {
		PhysicalComponent temp = PhysicalComponent(x, y, 0, 0);
		Tile *t = world->getPresentTile(&temp);
		if (!t->isOpen()) {
			return false;
		}
		if (error > 0) {
			x += xInc;
			error -= dy;
		} else {
			y += yInc;
			error += dx;
		}
	}
	return true;
}

bool AIComponent::clearShot(PhysicalComponent *p) {
	if (!level) {
		level = Level::getInstance();
	}
	std::pair<int,int> start = subject->getSpace()->getCenter();
	std::pair<int,int> end = p->getCenter();
	int dx = abs(end.first - start.first);
	int dy = abs(end.second - start.second);
	int x = start.first;
	int y = start.second;
	int n = 1 + dx + dy;
	int xInc = (end.first > start.first) ? 1 : -1;
	int yInc = (end.second > start.second) ? 1 : -1;
	int error = dx - dy;
	dx *= 2;
	dy *= 2;
	for (; n > 0; --n) {
		PhysicalComponent temp = PhysicalComponent(x, y, 1, 1);
		Tile *t = world->getPresentTile(&temp);
		if (!t->isOpen()) {
			return false;
		}
		if (level->isEnemyOccupied(temp, subject)) {
			return false;
		}
		if (error > 0) {
			x += xInc;
			error -= dy;
		} else {
			y += yInc;
			error += dx;
		}
	}
	return true;
}

void AIComponent::moveToTarget() {
	std::pair<int,int> end = target->getPosition();
	std::pair<int,int> start = subject->getSpace()->getPosition();
	int dx = abs(end.first - start.first);
	int dy = abs(end.second - start.second);
	if (dx >= dy) {
		dx = 1;
		dy = 0;
	} else if (dy > dx) {
		dx = 0;
		dy = 1;
	}
	int xInc = (end.first > start.first) ? 1 : -1;
	int yInc = (end.second > start.second) ? 1 : -1;
	subject->processInput(dx * xInc, dy * yInc);
	lastInput = std::make_pair(dx * xInc, dy * yInc);
}

Tile *AIComponent::getFreeTile(Tile *current) {
	Tile *neighbour = NULL;
	while (!neighbour) {
		std::pair<int,int> tilePos = current->getSpace()->getCenter();
		PhysicalComponent p = PhysicalComponent(tilePos.first, tilePos.second, 0, 0);
		std::pair<int,int> pos = p.getPosition();
		int r = rand() % 4;
		switch (r) {
			case 0:
				p.changePosition(pos.first + world->getTileSize(), pos.second);
			break;
			case 1:
				p.changePosition(pos.first - world->getTileSize(), pos.second);
			break;
			case 2:
				p.changePosition(pos.first, pos.second + world->getTileSize());
			break;
			case 3:
				p.changePosition(pos.first, pos.second - world->getTileSize());
			break;
		}
		if (!world->collision(p)) {
			neighbour = world->getPresentTile(&p);
		}
	}
	return neighbour;
}

std::stack<Tile *> AIComponent::findPath(Tile *start, Tile *end) {
	std::map<Tile *, int> open;
	std::vector<Tile *> closed;
	std::map<Tile *, Tile *> previous;
	Tile *current;
	open[start] = 0;
	while (current != end) {
		int min = open.begin()->second;
		current = open.begin()->first;
		std::map<Tile *, int>::iterator it;
		for (it = open.begin(); it != open.end(); ++it) {
			if (it->second < min) {
				min = it->second;
				current = it->first;
			}
		}
		open.erase(current);
		closed.push_back(current);
		for (int i = -1; i <= 1; i++) {
			PhysicalComponent p = PhysicalComponent(*current->getSpace());
			std::pair<int,int> pos = p.getPosition();
			p.changePosition(pos.first + i * world->getTileSize(), pos.second);
			Tile *neighbour = world->getPresentTile(&p);
			if (std::find(closed.begin(), closed.end(), neighbour) ==
				closed.end() && neighbour->isOpen()) {
				std::pair<int,int> nPos = neighbour->getSpace()->getPosition();
				std::pair<int,int> sPos = start->getSpace()->getPosition();
				std::pair<int,int> ePos = end->getSpace()->getPosition();
				int tentative = abs(nPos.first - sPos.first) +
								abs(nPos.second - sPos.second) +
								abs(nPos.first - ePos.first) +
								abs(nPos.second - ePos.second);
				if (open.count(neighbour) == 0) {
					open[neighbour] = tentative;
					previous[neighbour] = current;
				}
				if (tentative < open[neighbour]) {
					open[neighbour] = tentative;
					previous[neighbour] = current;
				}
			}
		}
		for (int i = -1; i <= 1; i++) {
			PhysicalComponent p = PhysicalComponent(*current->getSpace());
			std::pair<int,int> pos = p.getPosition();
			p.changePosition(pos.first, pos.second + i * world->getTileSize());
			Tile *neighbour = world->getPresentTile(&p);
			if (std::find(closed.begin(), closed.end(),neighbour) ==
				closed.end() && neighbour->isOpen()) {
				std::pair<int,int> nPos = neighbour->getSpace()->getPosition();
				std::pair<int,int> sPos = start->getSpace()->getPosition();
				std::pair<int,int> ePos = end->getSpace()->getPosition();
				int tentative = abs(nPos.first - sPos.first) +
								abs(nPos.second - sPos.second) +
								abs(nPos.first - ePos.first) +
								abs(nPos.second - ePos.second);
				if (open.count(neighbour) == 0) {
					open[neighbour] = tentative;
					previous[neighbour] = current;
				}
				if (tentative < open[neighbour]) {
					open[neighbour] = tentative;
					previous[neighbour] = current;
				}
			}
		}
	}
	std::stack<Tile *> path;
	while (current != start) {
		current = previous[current];
		path.push(current);
	}
	return path;
}
