#include "input.h"
#include <iostream>

bool Input::instanceFlag = false;
Input *Input::instance = NULL;

Input *Input::getInstance() {
	if (!instanceFlag) {
		instance = new Input();
		instanceFlag = true;
	}
	return instance;
}

Input::Input() {
	quit = false;
	dragging = false;
	player = NULL;
}

Input::~Input() {
	instanceFlag = false;
}

void Input::update() {
	while(SDL_PollEvent(&e) != 0) {
		int x = 0;
		int y = 0;
		int motionX = -1;
		int motionY = -1;
		if (e.type == SDL_QUIT) {
			quit = true;
		}
		if (e.type == SDL_KEYDOWN && e.key.repeat == 0) {
			switch (e.key.keysym.sym) {
				case SDLK_w: y--; break;
				case SDLK_s: y++; break;
				case SDLK_a: x--; break;
				case SDLK_d: x++; break;
				case SDLK_SPACE: player->dash(); break;
				case SDLK_q: player->switchPrimary(); break;
				case SDLK_e: player->switchSecondary(); break;
			}
		} else if (e.type == SDL_KEYUP && e.key.repeat == 0) {
			switch (e.key.keysym.sym) {
				case SDLK_w: y++; break;
				case SDLK_s: y--; break;
				case SDLK_a: x++; break;
				case SDLK_d: x--; break;
			}
		}
		if (e.button.state == SDL_PRESSED &&
			e.button.button == SDL_BUTTON_LEFT) {
			player->shoot(0);
		}
		if (e.button.state == SDL_RELEASED &&
			e.button.button == SDL_BUTTON_LEFT) {
			player->stop(0);
		}
		if (e.button.state == SDL_PRESSED &&
			e.button.button == SDL_BUTTON_RIGHT) {
			player->shoot(1);
		}
		if (e.button.state == SDL_RELEASED &&
			e.button.button == SDL_BUTTON_RIGHT) {
			player->stop(1);
		}
		if (e.type == SDL_MOUSEMOTION) {
			motionX = e.motion.x;
			motionY = e.motion.y;
			if (SDL_GetMouseState(NULL, NULL) == SDL_BUTTON(1)) {
				player->shoot(0);
			}
			if (SDL_GetMouseState(NULL, NULL) == SDL_BUTTON(3)) {
				player->shoot(1);
			}
		}
		Display *d = Display::getInstance();
		d->setCursor(motionX, motionY);
		player->getInput(x, y, d->getCursor());
	}
}

void Input::addPlayer(Player *p) {
	player = p;
}

bool Input::active() {
	return !quit;
}
