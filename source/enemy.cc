#include "enemy.h"

const std::string srcEnemy = "Assets/Textures/Animations/Enemy.png";
const std::string srcShield = "Assets/Textures/ShieldBar.png";

Enemy::Enemy(int size): Character(srcEnemy, size, -1) {
	shooting.push_back(false);
	int r = rand() % 3;
	Arm *g;
	switch (r) {
		case 0:
			g = new Machinegun(-1);
			break;
		case 1:
			g = new Shotgun(-1);
			break;
		case 2:
			g = new Pistol(-1);
			break;
	}
	gun.push_back(g);
	hit = false;
	health = new Object(srcShield);
}

bool Enemy::onTarget() {
	if (target) {
		return inside(*target);
	}
	return false;
}

bool Enemy::isHit() {
	return hit;
}

bool Enemy::collision(Object &o) {
	bool collide = Character::collision(o);
	if (collide && o.getHeight() == 3) {
		hit = true;
	}
	return collide;
}

void Enemy::render() {
	hit = false;
	Character::render();
	if (shieldCapacity < shieldMaxCapacity && shieldCapacity > 0) {
		float x = shieldCapacity;
		float y = shieldMaxCapacity;
		float ratio =  x / y;
		health->updateOnScreen(onScreen->x, onScreen->y, 50 * ratio, 5);
		health->render();
	}
}

void Enemy::stopMoving() {
	velX = 0;
	velY = 0;
}

bool Enemy::overheated() {
	std::vector<Arm *>::iterator it;
	for (it = gun.begin(); it < gun.end(); ++it) {
		if ((*it)->overheated()) {
			return true;
		}
	}
	return false;
}
