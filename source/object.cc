#include "object.h"
#include "display.h"
#include <cmath>

static SDL_Renderer *renderer;
const float pi = 3.14159265358;

Object::Object() {
	declarations();
}

Object::Object(std::string path): path(path) {
	declarations();
}

Object::~Object() {
	SDL_DestroyTexture(image);
	image = NULL;
	delete inGame;
	delete onScreen;
	delete source;
}

void Object::declarations() {
	image = NULL;
	inGame = NULL;
	onScreen = NULL;
	source = NULL;
	angle = 0;
	height = 0;
	alignment = 0;
	damage = 0;
}

void Object::initialize() {
	SDL_Surface *loadsurface = IMG_Load(path.c_str());
	SDL_SetColorKey(loadsurface, SDL_TRUE, SDL_MapRGB(loadsurface->format, 0x00, 0xFF, 0xFF));
	image = SDL_CreateTextureFromSurface(renderer, loadsurface);
	SDL_FreeSurface(loadsurface);
}

void Object::updateHeight(int h) {
	height = h;
}

int Object::getHeight() {
	return height;
}

void Object::rotate(float rotation) {
	angle += rotation;
	if (angle > 360) {
		angle -= 360;
	}
}

void Object::rotatePoint(float a, Object &o) {
	float s = sin(a);
	float c = cos(a);
	inGame->x -= o.inGame->x;
	inGame->y -= o.inGame->y;
	float x = inGame->x * c - inGame->y * s;
	float y = inGame->x * s + inGame->y * c;
	inGame->x = x + o.inGame->x;
	inGame->y = y + o.inGame->y;
}

void Object::updatePath(std::string p) {
	path = p;
	initialize();
}

void Object::updateInGame(int x, int y, int w, int h) {
	if (!inGame) {
		inGame = new SDL_Rect;
	}
	inGame->x = x;
	inGame->y = y;
	if (w >= 0 && h >= 0) {
		inGame->w = w;
		inGame->h = h;
	}
}

void Object::updateOnScreen(int x, int y, int w, int h) {
	if (!onScreen) {
		onScreen = new SDL_Rect;
	}
	onScreen->x = x;
	onScreen->y = y;
	if (w >= 0 && h >= 0) {
		onScreen->w = w;
		onScreen->h = h;
	}
}

void Object::updateSource(int x, int y, int w, int h) {
	if (!source) {
		source = new SDL_Rect;
	}
	source->x = x;
	source->y = y;
	if (w >= 0 && h >= 0) {
		source->w = w;
		source->h = h;
	}
}

void Object::pointTo(Object &target) {
	float x, y, targetX, targetY;
	x = onScreen->x + onScreen->w / 2;
	y = onScreen->y + onScreen->h / 2;
	if (target.onScreen) {
		targetX = target.onScreen->x + target.onScreen->w / 2;
		targetY = target.onScreen->y + target.onScreen->h / 2;
	} else {
		targetX = target.inGame->x + target.inGame->w / 2;
		targetY = target.inGame->y + target.inGame->h / 2;	
	}

	if (targetY > y) {
		angle = 180 + (atan((targetX - x) / (y - targetY)) * 180 / pi);
	} else if (targetY < y) {
		angle = -1 * (atan((x - targetX) / (y - targetY)) * 180 / pi);
	}
	if (angle < 0) {
		angle += 360;
	}
}

void Object::setScreenPos() {
	Display *d = Display::getInstance();
	Object *screen = d->getScreen();
	if (!inGame) {
		return;
	}
	if (!onScreen) {
		onScreen = new SDL_Rect;
	}
	onScreen->x = inGame->x - screen->inGame->x;
	onScreen->y = inGame->y - screen->inGame->y;
	onScreen->w = inGame->w;
	onScreen->h = inGame->h;
}

void Object::moveToDest(Object &dest) {
	if (inGame && dest.inGame) {
		inGame->x = dest.inGame->x;
		inGame->y = dest.inGame->y;
	}
}

void Object::moveToDestCenter(Object &dest) {
	if (inGame && dest.inGame) {
		inGame->x = dest.inGame->x + dest.inGame->w / 2;
		inGame->y = dest.inGame->y + dest.inGame->h / 2;
	}
}

void Object::move(int x, int y) {
	inGame->x += x;
	inGame->y += y;
}

void Object::render() {
	if (height <= 3) {
		setScreenPos();
	}
	if (image == NULL) {
		initialize();
	}
	if ((!source) && (!angle)) {
		SDL_RenderCopyEx(renderer, image, NULL, onScreen, 0, NULL, SDL_FLIP_NONE);
	} else if (!angle) {
		SDL_RenderCopyEx(renderer, image, source, onScreen, 0, NULL, SDL_FLIP_NONE);
	} else if (!source) {
		SDL_RenderCopyEx(renderer, image, NULL, onScreen, angle, NULL, SDL_FLIP_NONE);
	} else {
		SDL_RenderCopyEx(renderer, image, source, onScreen, angle, NULL, SDL_FLIP_NONE);
	}
}

bool Object::collision(Object &o) {
	if (inGame && o.inGame) {
		if (inGame->x + inGame->w >= o.inGame->x &&
			inGame->x <= o.inGame->x + o.inGame->w &&
			inGame->y + inGame->h >= o.inGame->y &&
			inGame->y <= o.inGame->y + o.inGame->h) {
			return true;
		}
	}
	return false;
}

bool Object::inside(Object &o) {
	if (inGame && o.inGame) {
		if (inGame->x >= o.inGame->x &&
			inGame->x + inGame->w <= o.inGame->x + o.inGame->w &&
			inGame->y >= o.inGame->y &&
			inGame->y + inGame->h <= o.inGame->y + o.inGame->h) {
			return true;
		}
	}
	return false;
}
