#ifndef __DAMAGECOMPONENT_H__
#define __DAMAGECOMPONENT_H__

class DamageComponent {
private:
	int damage;
public:
	DamageComponent(int);
	int getDamage();
};

#endif
